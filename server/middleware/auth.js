const { User } = require('../models/Users');
const jwt = require('jsonwebtoken');

const dotenv = require('dotenv/config');

exports.checkToken = async(req, res, next) => {
    try{
        if(req.headers['x-access-token']){
            const accessToken = req.headers['x-access-token'];
            const { _id, role, username, exp } = jwt.verify(accessToken, process.env.DB_SECRET);

            res.locals.userData = {_id, role, username, exp};
            
            next();
        }else{
            next();
        }
    }catch(error){
        return res.status(401).json({error:"Bad token", errors:error})
    }
}


exports.checkLoggedIn = (req, res, next) => {
    const user = res.locals.userData;
    if(!user) return res.status(401).json({error:"User not found. Please log in"})

    req.user = user;
    next();
}