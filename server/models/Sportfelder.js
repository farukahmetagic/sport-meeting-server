const mongoose = require('mongoose');

const SportfelderSchema = mongoose.Schema({
    name: String,
    bez: String,
    adresse:String
},
{collection: "sportfelder"});

module.exports = mongoose.model("Sportfelder", SportfelderSchema);