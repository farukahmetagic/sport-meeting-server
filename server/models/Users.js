const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const dotenv = require('dotenv/config');

const usersSchema = mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        trim:true,
        maxLength: 20,
        minLength: 5
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minLength: 5
    },
    role:{
        type:String,
        enum:['user','admin'],
        default:'user'
    }
});

usersSchema.pre('save', async function(next){
    let user = this;
    if(user.isModified('password')){
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(user.password, salt);
        user.password = hash;
    }
    next();
})

usersSchema.methods.generateToken = function(){
    let user = this; 
    const userObj = { _id:user._id.toHexString(), username:user.username, role:user.role};
    const token = jwt.sign(userObj,process.env.DB_SECRET,{ expiresIn:'1d'});
    return token;
}

usersSchema.methods.comparePassword = async function(candidatePassword){
    const user = this;
    const match = await bcrypt.compare(candidatePassword, user.password);
    return match;
}

usersSchema.statics.usernameTaken = async function(username){
    const user = await this.findOne({username});
    return !!user;
}

module.exports = mongoose.model("User", usersSchema); 
