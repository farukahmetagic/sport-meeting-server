const mongoose = require('mongoose');

const participantsSchema = mongoose.Schema({
    user:{type:String},
    team:{type:String}
})

const eventsSchema = mongoose.Schema({
    user: {
        type: String,
        default:''
    },
    sport: {
        type:String,
        default:''
    },
    bez:{
        type:String,
        default:''
    },
    address:{
        type:String,
        default:''
    },
    dateAndTime:{
        type:String,
        default:''
    },
    team:{
        type:String,
        default:''
    },
    participants:[participantsSchema]
});



module.exports = mongoose.model("Events", eventsSchema); 