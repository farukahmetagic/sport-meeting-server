const express = require('express');
const dotenv = require('dotenv/config');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3002;

app.use(express.json());

const usersRoute = require('./routes/users');
const {checkToken} = require('./middleware/auth')



const sportRoute = require('./routes/sportfelder');
const eventRoute = require('./routes/events');




mongoose.connect(process.env.CONNECTIONSTRING,{ 
    useNewUrlParser: true,
    useUnifiedTopology: true },()=>{
    console.log('connected to DB');
});

mongoose.set('useCreateIndex', true);

app.use(checkToken);

app.use('/users', usersRoute);
app.use('/sports', sportRoute);
app.use('/events', eventRoute);

app.use(express.static('client/build'));

if(process.env.NODE_ENV === 'production'){
    const path = require('path');
    app.get('/*',(req,res) => {
        res.sendFile(path.resolve(__dirname,'../client','build','index.html'))
    })
}

app.listen(PORT);