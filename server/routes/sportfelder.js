const express = require('express');
const router = express.Router();
const Sportfelder = require('../models/Sportfelder')

// gets all sports fields
router.get('/', async (req,res)=>{
    try{
        const sportfelder = await Sportfelder.find(); 
        res.json(sportfelder)
    }catch(err){
        res.json({message:err})
    }
});

// specific sports field
router.get('/:sportName', async (req,res) => {
    try{
    const sportfeld = await Sportfelder.find({name: req.params.sportName});
    res.json(sportfeld);
    }catch(err){
        res.json({message: err});
    }
});

// specific sports field and bezirk
router.get('/:sportName/:bezirk', async (req,res) => {
    try{
    const sportfeld = await Sportfelder.find({
        name: req.params.sportName,
        bez: req.params.bezirk
    });
    res.json(sportfeld);
    }catch(err){
        res.json({message: err});
    }
});


module.exports = router;