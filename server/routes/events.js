const express = require('express');
const router = express.Router();
const dotenv = require('dotenv/config');
const Events = require('../models/Events');
const {checkLoggedIn} = require('../middleware/auth');
const { grantAccess } = require('../middleware/roles');

// gets all events
router.post('/', async (req,res)=>{
    try{
        const events = await Events.find({sport:req.body.sport}); 
        res.json(events)
    }catch(err){
        res.json({message:err})
    }
});

// get events from one user
router.get('/myevents',checkLoggedIn,grantAccess('readOwn','profile'), async(req,res) => {
    try{
        const events = await Events.find({user:req.user.username});

        res.status(200).json(events);
    }catch(error){
        return res.status(400).send(error);
    }
    
})
/////////////////////// GET JOINT EVENTS /////////////////////////////////////
router.get('/myevents/jointevents',checkLoggedIn, async(req,res) => {
    try{
        const events = await Events.find({"participants.user":req.user.username});

        res.status(200).json(events);
    }catch(error){
        return res.status(400).send(error);
    }
    
})
///////////////////////// MAKE NEW EVENT ////////////////////////////
.post('/myevents/newevent',checkLoggedIn,grantAccess('readOwn','profile'), async (req,res) => {
    try{        
        const event = new Events({                
                user:req.body.user,
                sport:req.body.sport,
                bez:req.body.bez,
                address:req.body.address,
                dateAndTime:req.body.dateAndTime,
                team:req.body.team                
        });
        const savedDate = await event.save();
        res.status(200).json(getEvents(savedDate));
        
    }catch(error){
        res.status(400).json({message:'Problem updating',error:error});
    }
}) 

/////////////////////// DELETE OWN EVENT /////////////////////////////////////////////////
router.post('/myevents/delete',checkLoggedIn,grantAccess('deleteOwn','profile'), async (req,res) => {
    try{
        const event = await Events.findByIdAndRemove(req.body._id);
        if(!event) return res.status(400).json({message:'Event not found!'})

        res.status(200).send('Event deleted')
    }catch(error){
        res.status(400).json({message:error});
    }
})

//////////////////////// DELETE JOINT EVENT /////////////////////////////////////////
router.post('/myevents/deletejoint',checkLoggedIn, async (req,res) => {
    try{
        const event = await Events.findByIdAndUpdate(req.body._id,
            {$pull:{participants:{user:req.body.user}}},
            {multi:true})
            
        if(!event) return res.status(400).json({message:'Event not found!'})

        res.status(200).send('Participation canceled')
    }catch(error){
        res.status(400).json({message:error});
    }
})
/////////////////////// DELETE ALL EVENTS FROM ONE USER /////////////////////////////////////////////////
router.post('/myevents/deleteall',checkLoggedIn,grantAccess('deleteOwn','profile'), async (req,res) => {
    try{
        const event = await Events.deleteMany({user:req.user.username});
        if(!event) return res.status(400).json({message:'Events not found!'})

        res.status(200).send('Events deleted')
    }catch(error){
        res.status(400).json({message:error});
    }
})

///////////////////////// ADD PARTICIPANTS /////////////////////////////////////////////////

router.post('/participate',checkLoggedIn, async(req,res) => {
    try{
        const event = await Events.findOneAndUpdate({_id:req.body._id},{
            $push:{
                participants:{
                    user:req.body.user,
                    team:req.body.team
                }
            }
        });
        if(!event) return res.status(400).json({message:'Event not found!'})

        res.status(200).json(getEvents(event));

    }catch(error){
        res.status(400).json({message:error})
    }
})

const getEvents = (user) => {
    return{
        _id:user._id,
        user:user.user,
        sport:user.sport,
        bez:user.bez,
        address:user.address,
        dateAndTime:user.dateAndTime,
        team:user.team,
        participants:user.participants
    }
}



module.exports = router;