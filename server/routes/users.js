const express = require('express');
const router = express.Router();
const dotenv = require('dotenv/config');
const {checkLoggedIn} = require('../middleware/auth');
const { grantAccess } = require('../middleware/roles');
const User = require('../models/Users');
const bcrypt = require('bcrypt');


// gets all users
router.get('/', async (req,res)=>{
    try{
        const users = await User.find(); 
        res.json(users)
    }catch(err){
        res.json({message:err})
    }
});

// Sign in user
router.post('/signin', async (req,res) => {
    try{
    const user = await User.findOne({
        username: req.body.username
    });
    if(!user) return res.status(400).json({message:'Bad username'});

    const compare = await user.comparePassword(req.body.password);
    if(!compare) return res.status(400).json({message:'Bad password'});

    const token = user.generateToken();

    res.cookie('x-access-token', token)
        .status(200).send(getUserProps(user));
    }catch(err){
        res.status(400).json({message: 'Error', error: err});
    }
});

// register user
router.post('/register', async (req,res) => {
    
    try{
        if(await User.usernameTaken(req.body.username)){
            return res.status(400).json({message:'Sorry Username taken'})
        }
        const user = new User({
            username: req.body.username,
            password: req.body.password
        });
        const token = user.generateToken();
        const savedUser = await user.save();
        res.cookie('x-access-token', token)
        .status(200).send(getUserProps(savedUser));
    }catch(err){
        res.status(400).json({message: 'Error', error: err});
    }
})


// USERS PROFILE
router.post('/profile',checkLoggedIn,grantAccess('readOwn','profile'), async(req,res) => {
    try{
        const user = await User.findById(req.user._id);

        res.status(200).json(user);
    }catch(error){
        return res.status(400).send(error);
    }
    
})
////////////////////////// update username //////////////////////////////////////////////
.patch('/profile/username',checkLoggedIn,grantAccess('updateOwn','profile'), async (req,res) => {
    try{
        if( await User.usernameTaken(req.body.username)){
            return res.status(400).json({message:'Sorry Username taken'})
        }
        const user = await User.findOneAndUpdate(
            {_id: req.user._id},
            {
                "$set":{
                    username:req.body.username
                }
            },
            {new: true}
        );
        if(!user) return res.status(400).json({message:'User not found'})

        const token = user.generateToken();

        res.cookie('x-access-token', token)
           .status(200).send({username:user.username})
    }catch(error){
        res.status(400).json({message:'Problem updating',error:error});
    }
})
////////////////////////// update password //////////////////////////////////////////////
.patch('/profile/password',checkLoggedIn,grantAccess('updateOwn','profile'), async (req,res) => {
    try{
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(req.body.password, salt);
        req.body.password = hash;
        const user = await User.findOneAndUpdate(
            {_id: req.user._id},
            {
                "$set":{
                    password:req.body.password
                }
            },
            {new: true}
        );
        if(!user) return res.status(400).json({message:'User not found'})

        const token = user.generateToken();

        res.cookie('x-access-token', token)
        .status(200).send(getUserProps(user));
    }catch(error){
        res.status(400).json({message:'Problem updating',error:error});
    }
})

/////////////////////// DELETE PROFILE /////////////////////////////////////////////////
router.post('/profile/delete',checkLoggedIn,grantAccess('deleteOwn','profile'), async (req,res) => {
    try{
        const user = await User.findOneAndRemove(
            {_id: req.user._id}
        );
        if(!user) return res.status(400).json({message:'User not found'})

        res.status(200).send('User deleted')
    }catch(error){
        res.status(400).json({message:error});
    }
})


router.get('/isauth',checkLoggedIn,async(req,res) => {
    res.status(200).send(getUserProps(req.user))
})

const getUserProps = (user) => {
    return{
        _id: user._id,
        username: user.username,
        role: user.role,
        sport:user.sport,
        bez:user.bez,
        address:user.address,
        dateAndTime:user.dateAndTime,
        team:user.team
    }
}

module.exports = router;