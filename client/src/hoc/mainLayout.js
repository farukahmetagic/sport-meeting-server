import React,{ useState } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import KeyboardArrowUpRoundedIcon from '@material-ui/icons/KeyboardArrowUpRounded';
import { Fab } from '@material-ui/core';




const MainLayout = (props) => {

    const handleClick=()=>{
        window['scrollTo']({top:0, behavior: 'smooth'})
    }

    const [ show, setShow] = useState(0);

    window.addEventListener("scroll",()=>{
        if(window.pageYOffset > 100) {
            setShow(1);
        }else{
            setShow(0);
        }
    })

    return(
        <div className={`app_container mb-5`}>
            {props.children}
            <ToastContainer/>           
            <Fab color="primary" aria-label="add" size="small" 
              style={{
                position:"fixed",
                bottom:5,
                right:5,
                zIndex:99,
                opacity:show,
                transition: ".4s"
              }} 
              onClick={handleClick}           
            >
            
                <KeyboardArrowUpRoundedIcon/>
            </Fab>
        </div>
    )
}

export default MainLayout;