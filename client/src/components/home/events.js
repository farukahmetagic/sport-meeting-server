import React,{useState} from 'react';
import { 
  makeStyles,
  Card,
  CardContent,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText      
} from '@material-ui/core';
import SportsBaseballIcon from '@material-ui/icons/SportsBaseball';
import EventIcon from '@material-ui/icons/Event';
import StreetviewRoundedIcon from '@material-ui/icons/StreetviewRounded';
import PeopleRoundedIcon from '@material-ui/icons/PeopleRounded';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';

import { addParticipant, getAllEvents } from '../../store/actions/events_actions';
import { useDispatch, useSelector } from 'react-redux';

const Events = () => {

  const getTeam = () => {
    let selectItems = [];
    for(let i=1; i<=5; i++){
      selectItems.push(
        <MenuItem
          key={`id${i}`}
          value={`${i} Players`}
        >{`${i} Players`}
        </MenuItem>
      )
    }
    return selectItems;
  }   
  
  const events = useSelector( state => state.events );
  const user = useSelector( state => state.users );

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const dispatch = useDispatch();

   const convertDate = (date) => {
     let d = new Date(date);
     return(
       d.toLocaleString()
     )
   }
    const useStyles = makeStyles({
      formControl: {
        minWidth: 120,
        margin: "1% 0 2% 0"
      },
      root: {
        minWidth: 275,
        marginBottom:20
      },
      pos: {
        fontSize: 14,
        marginBottom: 12
      },
      wrapper: {
        display: "flex",
        flexDirection: "column",
        textAlign: 'center'
      }
    });
    const classes = useStyles();

    const [data, setData] = useState({
      _id:'',
      user:user.data.username,
      team:''
    });
    
    const handleChangeTeam = (event) => {
      setData({
        ...data,
        team:event.target.value
      })
    }

    const handleSubmit = (sport) => {
      dispatch(addParticipant(data));
      dispatch(getAllEvents(sport));
      handleClose()
    }
    
    const getEvents = () => {
      return(
        events.data.map((item) => (  
          <Card className={classes.root} key={item._id}>
            <CardContent>
              <div>        
            <List>
                <ListItem>
                  <ListItemAvatar>
                      <SportsBaseballIcon/>
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.sport==='fussball'?
                    'football'
                    :
                    item.sport}
                  />
                </ListItem>
                <ListItem>
                  <ListItemAvatar>
                      <AccountCircleRoundedIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.user}
                  />
                </ListItem>
                <ListItem>
                  <ListItemAvatar>
                      <StreetviewRoundedIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={`${item.address}, ${item.bez}, Wien`}
                  />
                </ListItem>

                <Divider/>

                <ListItem>
                  <ListItemAvatar>
                      <EventIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      convertDate(item.dateAndTime)
                    }
                  />
                </ListItem>

                <Divider/>

                <ListItem>
                  <ListItemAvatar>
                      <PeopleRoundedIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={`Team: ${item.team}`}
                  />
                </ListItem>                
                <Divider/>

                  Participants:               
                  {item.participants.map((items)=>(
                    <ListItem key={items._id}>
                      <ListItemAvatar>
                      <AccountCircleRoundedIcon />
                      </ListItemAvatar>
                      <ListItemText
                        primary={`${items.user} , Team: ${items.team}`}                         
                      />
                    </ListItem> 
                  ))
                  }
                  
                              
            </List>             
            </div>
            </CardContent>
            <Divider/>
            <CardContent style={{textAlign:"center"}}>
                <Button 
                  className="mt-3"
                  variant="contained" 
                  color="primary" 
                  size="large" 
                  onClick={()=> {
                    handleClickOpen();
                    setData({
                      ...data,
                      _id:item._id
                    })
                  }
                  }
                  disabled={(user.data.username===item.user || item.participants.some(items => items.user === user.data.username)) ? true : false}                  
                >
                    Participate
                </Button>
            </CardContent> 
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="responsive-dialog-title"
            >
              <DialogTitle id="responsive-dialog-title">{"Participate Event"}</DialogTitle>
              <DialogContent>
                <FormControl className={classes.formControl}>
                  <InputLabel id="teams">Team</InputLabel>
                  <Select 
                    labelId="teams" 
                    id="select-team" 
                    value={data.team} 
                    onChange={handleChangeTeam}
                  >  
                    <MenuItem value="tennis" disabled>Team</MenuItem>
                    { getTeam ()}
                  </Select>
                  <FormHelperText>Max 5 Players. This field is optional</FormHelperText> 
                </FormControl>
              </DialogContent>
              <DialogActions>
                <Button autoFocus onClick={handleClose} color="primary">
                    Do not participate
                </Button>
                <Button onClick={()=>handleSubmit(item.sport)} color="primary" autoFocus>
                    Participate
                </Button>
              </DialogActions>
            </Dialog>           
          </Card>  
        ))
      )
    }
    return(
        <>
            {events.hasOwnProperty('data') ?
              getEvents()
              :
              null
            }     
        </>
    )
}

export default Events; 