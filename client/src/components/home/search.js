import React from 'react';
import { useDispatch } from 'react-redux';
import { getAllEvents } from '../../store/actions/events_actions';

import {Card, CardContent, makeStyles, IconButton } from '@material-ui/core';
import SportsSoccerRoundedIcon from '@material-ui/icons/SportsSoccerRounded';
import SportsBasketballRoundedIcon from '@material-ui/icons/SportsBasketballRounded';
import SportsTennisRoundedIcon from '@material-ui/icons/SportsTennisRounded';

const Search = (props) => {
  
  const useStyles = makeStyles({
    root: {
      minWidth: 275,
      marginBottom:20
    },
    pos: {
      fontSize: 14,
      marginBottom: 12
    },
    formControl: {
      minWidth: 120,
      margin: "1% 0 2% 0"
    },
    titleText:{
      color:'#81D11A',
      marginTop:"10%"
    }
  });
  const classes = useStyles();

  const dispatch = useDispatch();

  const getAll = (sport) => {
    dispatch(getAllEvents(sport))
  }

  
  
  return(
    <div {...props} style={{textAlign:"center"}}>        
        <h2 className={classes.titleText}>Sports range</h2>        
        <Card className={classes.root} onClick={props.handleChange}>
          <CardContent  onClick={() => {getAll('fussball')}}>
            <IconButton >
                <SportsSoccerRoundedIcon/>
                Football
            </IconButton>
          </CardContent>           
        </Card>      
        <Card className={classes.root} onClick={props.handleChange}>
          <CardContent onClick={() => {getAll('basketball')}}>
            <IconButton >
                <SportsBasketballRoundedIcon/>
                Basketball
            </IconButton>
          </CardContent>           
        </Card>      
        <Card className={classes.root} onClick={props.handleChange}>
          <CardContent onClick={() => {getAll('tennis')}}>
            <IconButton>
                <SportsTennisRoundedIcon/>
                Tennis
            </IconButton>
          </CardContent>           
        </Card> 
    </div>   
           
  )
}

export default Search;