import React,{useState} from 'react';
import { useSelector } from 'react-redux';

import {Collapse, Container, Card, IconButton } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';


import Search from './search';
import Events from './events';

const Home = (props) => {

  const [checked, setChecked] = useState(true);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };
  
  const users = useSelector(state=> state.users);
  const redirect = () => {
    if(!users.auth){
      props.history.push('/auth');
    }
  }
  
  return(
    <>        
      <Container>
        {redirect()}
        <Collapse in={!checked}>
          <Card onClick={handleChange} style={{marginBottom:20,marginTop:15, background:"#3f51b5"}}>          
            <IconButton style={{color:'#fff'}}>
              Search again <KeyboardArrowDownIcon/>
            </IconButton>           
          </Card>
          <Events/> 
        </Collapse>
        <Collapse  in={checked}>
          <Search handleChange={handleChange}/>
        </Collapse >      
      </Container>
    </>
  )
}

export default Home;