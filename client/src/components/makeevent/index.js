import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { submitMeeting, sportsFields, dis } from '../../store/actions/users_actions';
import { noEvent, noEvents } from '../../store/actions/events_actions';
import { errorGlobal } from '../../store/actions/index';

import ChooseSport from './choosesport';
import DateNteam from './dateNteam';

import {
  makeStyles, 
  Container,
  Button,
  Card,
  CardContent
 } from '@material-ui/core';


const MakeEvent = (props) => {
    
    const users = useSelector(state=> state.users);

    const redirect = () => {
      if(!users.auth){
        props.history.push('/auth');
      }
    }

    ///////////////////////// STYLES ////////////////////
    const useStyles = makeStyles(() => ({
      formControl: {
        minWidth: 120,
        margin: "1% 0 2% 0"
      },
      wrapper: {
        display: "flex",
        flexDirection: "column",
        textAlign: 'center'
      }
    }));   
    const classes = useStyles();    
    //////////////////////////////////////////////////////

    const [values, setValues] = useState({
      user:users.data.username,
      sport:'',
      district:'',
      address:'',
      selectedDate:new Date(),
      team:'No Team'
    });

    const dispatch = useDispatch();

    ////// HANDLE CHANGES FOR choosesport COMPONENT //////
    const handleChangeSports = (event) => {
        setValues({
          ...values,
          sport:event.target.value});
        dispatch(sportsFields(event.target.value));
      };
    const handleChangeDistricts = (event) => {
        setValues({
          ...values,
          district:event.target.value});
        dispatch(dis(values.sport, event.target.value))
    }

    const handleChangeAdress = (event) => {
        setValues({
          ...values,
          address:event.target.value});
    } 

    ////// HANDLE CHANGES FOR dateNtime COMPONENT ///////

    const handleDateChange = (date) => {
        setValues({
          ...values,
          selectedDate:date});
      };
      const handleChangeTeam = (event) => {
        setValues({
          ...values,
          team:event.target.value
        })
      }

    /////////////////////////////////////////////////////////  
    
    
    
    /////////////////// Main Submit /////////////////////////
    let todaysDate = new Date();
    const handleSubmit = (e) => {
      if(todaysDate<values.selectedDate){
        e.preventDefault();
        dispatch(submitMeeting(values));
        dispatch(noEvent());
        dispatch(noEvents());
        props.history.push('/');
      }else{
        e.preventDefault();
        dispatch(errorGlobal('Invalid Date! Date must be from tomorrow'))
      }
      
    } 
    
    const [choosesport, setChoosesport] = useState(false);
    
    //////////////////// Toggle Components (dateNtime and choosesport) ///////////////
    const toggleComp = (e) => {
        e.preventDefault();
        if(values.sport === '' || values.district === '' || values.address === ''){
          dispatch(errorGlobal('All fields must be selected'))
        }else{
          setChoosesport(!choosesport);
        }    
        
    }
    ////////////////////////////////////////////////////////////////////////////////////
        
    return(
      <>
        {redirect()}
        
        <Container  maxWidth="sm"
          id="container"
           style={{fontFamily:'Fredoka One',marginTop:"92px"}}
        >
          <Card><CardContent>
          <form className={classes.wrapper} onSubmit={handleSubmit}>
              { choosesport ?
                <DateNteam values={{values, handleDateChange, handleChangeTeam, classes}}/>
                :
                <ChooseSport values={{values, handleChangeSports, handleChangeDistricts, handleChangeAdress, classes}}/>
              }          
          <div>
            <Button 
                className={`mt-3 ${classes.btn}`}
                variant="outlined" 
                color="primary" 
                size="medium"  
                onClick={toggleComp}              
                >
                { !choosesport ?
                   'Date and Teams'
                   :
                   'Sports and fields'
                }
              </Button> 
          </div >
          </form> 
          </CardContent></Card>         
        </Container>
      </>
    )
    
}

export default MakeEvent;