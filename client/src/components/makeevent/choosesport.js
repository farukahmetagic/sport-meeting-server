import React from 'react';
import { useSelector } from 'react-redux';

import { 
  FormControl,
  Select,
  MenuItem,
  InputLabel, 
  FormHelperText,
 } from '@material-ui/core';

const ChooseSport = (props) => {

    const Values = props.values;
    
    const sports = useSelector( state => state.sports);
    
    // bezirk
    const getUnique = (arr, comp) => {
        const unique = arr
              .map(e => e[comp])
              .map((e, i, final) => final.indexOf(e) === i && i)
              .filter(e => arr[e])
              .map(e => arr[e]);
            return unique;
    };
    
    const getDistricts = () => {
    
      const getUniqueDistrict = getUnique(sports.data, 'bez');
      return(
        getUniqueDistrict.map((item) => (
          
          <MenuItem
          key={item._id}
          value={item.bez}
          >{item.bez}</MenuItem>
        ))
        
      )
    }
    // Adresse
    const getAdress = () => {
      return(
        sports.data.map((item) => (
          <MenuItem
          key={item._id}
          value={item.adresse}
          >{item.adresse}</MenuItem>
        ))
        
      )
    }      
      
    return(
      <>        
          <h2>Choose where to play</h2>
            <FormControl className={Values.classes.formControl} >
              <InputLabel id="sports">Sport Art</InputLabel>
              <Select 
                required
                labelId="sports" 
                id="select-sports" 
                value={Values.values.sport}
                onChange={Values.handleChangeSports}
              >
                <MenuItem id="fussball" value="fussball">Football</MenuItem>
                <MenuItem id="basketball" value="basketball">Basketball</MenuItem>
                <MenuItem id="tennis" value="tennis">Tennis</MenuItem>
              </Select>
              <FormHelperText>Choose sport you want to play</FormHelperText>
            </FormControl>
            <FormControl className={Values.classes.formControl}>
              <InputLabel id="districts">District</InputLabel>
              <Select 
                labelId="districts" 
                id="select-districts" 
                value={Values.values.district} 
                onChange={Values.handleChangeDistricts}
                required
              >  
                <MenuItem value="tennis" disabled>District</MenuItem>
                { sports.hasOwnProperty('data') ?
                    getDistricts()
                    :
                    null  
                }
              </Select>
              <FormHelperText>Choose district</FormHelperText>
            </FormControl>
    
            <FormControl className={Values.classes.formControl}>
              <InputLabel id="Addresses">Address</InputLabel>
              <Select 
                labelId="Addresses" 
                id="select-address" 
                value={Values.values.address} 
                onChange={Values.handleChangeAdress}
                required
              >  
                <MenuItem value="tennis" disabled>Address</MenuItem>
                { sports.hasOwnProperty('data') ?
                    getAdress()
                    :
                    null  
                }
              </Select>
              <FormHelperText>Choose the address</FormHelperText>            
            </FormControl>        
      </>
    )
    
}

export default ChooseSport;