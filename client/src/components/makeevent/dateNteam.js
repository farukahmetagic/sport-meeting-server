import React from 'react';

import { 
  FormControl,
  Select,
  MenuItem,
  InputLabel, 
  FormHelperText,
  Button
 } from '@material-ui/core';

 import 'date-fns';  
 import DateFnsUtils from '@date-io/date-fns';
 import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
 } from '@material-ui/pickers';

const DateNteam = (props) => {

    const Values = props.values;
    
    const getTeam = () => {
      let selectItems = [];
      for(let i=1; i<=5; i++){
        selectItems.push(
          <MenuItem
            key={`id${i}`}
            value={`${i} Players`}
          >{`${i} Players`}
          </MenuItem>
        )
      }
      return selectItems;
    }   

    return(
      <>          
        <h2>Choose date and time</h2>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              className={Values.classes.formControl}
              margin="normal"
              id="date-picker-dialog"
              label="Date"
              format="MM/dd/yyyy"
              value={Values.values.selectedDate}
              onChange={Values.handleDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
            <KeyboardTimePicker
              className={Values.classes.formControl}
              margin="normal"
              id="time-picker"
              label="Time"
              value={Values.values.selectedDate}
              onChange={Values.handleDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change time',
              }}
            />
        </MuiPickersUtilsProvider>
        <br/>
        <h2>Make your team</h2>
        <FormControl className={Values.classes.formControl}>
          <InputLabel id="teams">Team</InputLabel>
          <Select 
            labelId="teams" 
            id="select-team" 
            value={Values.values.team} 
            onChange={Values.handleChangeTeam}
          >  
            <MenuItem value="tennis" disabled>Team</MenuItem>
            { getTeam ()}
          </Select>
          <FormHelperText>Max 5 Players. This field is optional</FormHelperText> 
        </FormControl>
        <Button 
          className="mt-3"
          variant="contained" 
          color="primary" 
          size="large"
          type="submit"              
          >
          Submit event
        </Button> 
      </>
    )
    
}

export default DateNteam;