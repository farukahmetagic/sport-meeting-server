import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

import {
    Drawer,
    List,
    Divider,
    ListItem,
    ListItemIcon,
    ListItemText,
} from '@material-ui/core';

import DehazeIcon from '@material-ui/icons/Dehaze';
import HomeIcon from '@material-ui/icons/Home';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import TuneRoundedIcon from '@material-ui/icons/TuneRounded';
import SportsHandballRoundedIcon from '@material-ui/icons/SportsHandballRounded';

const SideDrawer = ({signOutUser}) => {
    
    const [state, setState] = useState(false);
    
    const users = useSelector(state => state.users);    

    return(
        <>
            <DehazeIcon
                className="drawer_btn"
                onClick={() => setState(true)}
            />
            <Drawer anchor={'right'} open={state} onClose={() => setState(false)}>
                <h5 style={{
                    margin:'20px',
                    fontFamily: 'Fredoka One'}}>SportMeeting</h5>
                <Divider/>
                <List>
                    <ListItem button component={RouterLink} to="/" onClick={()=> setState(false)}>
                        <ListItemIcon><HomeIcon/></ListItemIcon>
                        <ListItemText primary="Home"/>
                    </ListItem>

                    <ListItem button component={RouterLink} to="/makeevent" onClick={()=> setState(false)}>
                        <ListItemIcon><SportsHandballRoundedIcon/></ListItemIcon>
                        <ListItemText primary="New Playdate"/>
                    </ListItem>

                    {!users.auth ? 

                        <ListItem button component={RouterLink} to="/auth" onClick={()=> setState(false)}>
                            <ListItemIcon><VpnKeyIcon/></ListItemIcon>
                            <ListItemText primary="Sign in/up"/>
                        </ListItem>                        
                        :
                        <ListItem button onClick={()=> {
                            signOutUser()
                            setState(false)
                            }}>
                            <ListItemIcon><VpnKeyIcon/></ListItemIcon>
                            <ListItemText primary="Sign out"/>
                        </ListItem>
                    }  
                     
                </List>
                <Divider/>
                <List>
                { users.auth ? 

                    <ListItem button component={RouterLink} to="/settings" onClick={()=> setState(false)}>
                        <ListItemIcon><TuneRoundedIcon/></ListItemIcon>
                        <ListItemText primary="Settings"/>
                    </ListItem>                        
                    :
                    null
                }  
                </List>
            </Drawer>
        </>
    )
}

export default SideDrawer;