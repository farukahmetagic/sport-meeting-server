import React, {  } from 'react';
import { useFormik } from 'formik'
import * as Yup from 'yup';

import { useDispatch } from 'react-redux';
import { updatePassword } from '../../store/actions/users_actions'
import { TextField, Button, Container, makeStyles, FormHelperText } from '@material-ui/core';


const UpdatePassword = () => {

    const dispatch = useDispatch();

    const formik = useFormik({
        initialValues:{password:''},
        validationSchema:Yup.object({
            password:Yup.string()
            .max(20, 'Must be 20 characters or less')
            .min(5, 'Must be 5 characters or more')
        }) ,
        onSubmit:(values,{resetForm})=>{
            handleSubmit(values)
        }
    });

    ///////////// STYLES //////////////////

    const useStyles = makeStyles(() => ({
        formControl: {
            textAlign: 'center'
        }
      }));   
    const classes = useStyles();    
    //////////////////////////////////////
    
    const handleSubmit = (values) => {
        dispatch(updatePassword(values))
    }

    

    const errorHelper = (formik, values) => ({
        error:formik.errors[values] && formik.touched[values] ? true:false,
        helperText:formik.errors[values] && formik.touched[values] ? formik.errors[values] : null
    });
    return(
        <>  
           <Container maxWidth="sm" className={classes.formControl} >
                <form className="mt-3" onSubmit={formik.handleSubmit} key="updateUsername">
                    <div className="form-group">
                        <TextField
                            style={{width:'100%'}}
                            name="password"
                            label="New Password"
                            variant="outlined"
                            type="password"
                            {...formik.getFieldProps('password')}
                            {...errorHelper(formik,'password')}
                        />
                        <FormHelperText>Enter new password</FormHelperText>
                    </div>    
                    <Button 
                        className="mt-3" 
                        variant="contained" 
                        color="primary" 
                        type="submit"
                        size="large"
                        >
                            Update Password
                    </Button> 
                </form>  
                               
           </Container>
        </>
    )
}

export default UpdatePassword;