import React from 'react';
import { 
  makeStyles,
  Card,
  CardContent,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Container      
} from '@material-ui/core';
import SportsBaseballIcon from '@material-ui/icons/SportsBaseball';
import EventIcon from '@material-ui/icons/Event';
import StreetviewRoundedIcon from '@material-ui/icons/StreetviewRounded';
import PeopleRoundedIcon from '@material-ui/icons/PeopleRounded';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';

import { deleteEvent } from '../../store/actions/events_actions';
import { useDispatch } from 'react-redux';

const UpdateEvent = ({event}) => {

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const dispatch = useDispatch();

  const delEvent = (_id) => {
    dispatch(deleteEvent(_id));
    handleClose();
  }

   const convertDate = (date) => {
     let d = new Date(date);
     return(
       d.toLocaleString()
     )
   }
    const useStyles = makeStyles({
      root: {
        minWidth: 275,
        marginBottom:20
      },
      pos: {
        fontSize: 14,
        marginBottom: 12
      },
      formControl: {
        minWidth: 120,
        margin: "1% 0 2% 0"
      },
      wrapper: {
        display: "flex",
        flexDirection: "column",
        textAlign: 'center'
      }
    });
    const classes = useStyles();
    
    const getEvents = () => {
      return(
        event.data.map((item) => (  
          <Card className={classes.root} key={item._id}>
            <CardContent>
              <div>        
            <List>
                <ListItem>
                  <ListItemAvatar>
                      <SportsBaseballIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.sport==='fussball'?
                    'football'
                    :
                    item.sport}
                  />
                </ListItem>
                <ListItem>
                  <ListItemAvatar>
                      <StreetviewRoundedIcon />
                  </ListItemAvatar>
                  <ListItemText
                     primary={`${item.address}, ${item.bez}, Wien`}
                  />
                </ListItem>
                <Divider/>

                <ListItem>
                  <ListItemAvatar>
                      <EventIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      convertDate(item.dateAndTime)
                    }
                  />
                </ListItem>

                <Divider/>

                <ListItem>
                  <ListItemAvatar>
                      <PeopleRoundedIcon />
                  </ListItemAvatar>
                  <ListItemText
                    primary={`Team: ${item.team}`}
                  />
                </ListItem>  
                <Divider/>

                  Participants:               
                  {item.participants.map((items)=>(
                    <ListItem key={items._id}>
                      <ListItemAvatar>
                      <AccountCircleRoundedIcon />
                      </ListItemAvatar>
                      <ListItemText
                        primary={`${items.user} , Team: ${items.team}`}                         
                      />
                    </ListItem> 
                  ))
                  }              
            </List>             
            </div>
            </CardContent>
           
            <Divider/>
            <CardContent style={{textAlign:"center"}}>
                <Button 
                  className="mt-3"
                  variant="contained" 
                  color="primary" 
                  size="large" 
                  onClick={()=> {
                    handleClickOpen()
                    }
                  }
                >
                  Cancel Event
                </Button>
            </CardContent> 
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="responsive-dialog-title"
            >
              <DialogTitle id="responsive-dialog-title">{"Event delete"}</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Are you sure you want to delete event? It will be permanently deleted!
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button autoFocus onClick={handleClose} color="primary">
                  Disagree
                </Button>
                <Button onClick={()=>{delEvent(item._id)}} color="primary" autoFocus>
                  Agree
                </Button>
              </DialogActions>
            </Dialog>           
          </Card>  
        ))
      )
    }
    return(
        <Container>
            {event.hasOwnProperty('data') ?
              getEvents()
              :
              null
            }     
        </Container>
    )
}

export default UpdateEvent; 