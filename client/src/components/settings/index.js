import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
    AppBar,
    Tabs,
    Tab,
    Typography,
    Box,
    Container,
    Divider,
    Card,
    CardContent
} from '@material-ui/core';

import UpdateUsername from './updateusername';
import UpdatePassword from './updatepassword';
import DeleteAccount from './deleteaccount';
import UpdateEvent from './updateEvent';
import JointEvents from './jointevents';

import { useDispatch, useSelector } from 'react-redux';
import { getEvent,getJointEvents } from '../../store/actions/events_actions';

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography  component={'span'} variant={'body2'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

export default function ScrollableTabsButtonAuto( props ) {

  const event = useSelector(state => state.event);
  const users = useSelector(state=> state.users);

  const dispatch = useDispatch();

  const setResponsiveness = () => {
    return window.innerWidth < 700
      ? setMobileView(true)
      : setMobileView(false)
  };
    
  useEffect(()=>{
    if(event && !event.data){
      dispatch(getEvent())
      dispatch(getJointEvents())      
    }
    setResponsiveness()
  },[dispatch, event])   
      
   const redirect = () => {
     if(!users.auth){
       props.history.push('/auth');
     }
   }
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  }; 

  const [mobileView, setMobileView] = useState(true);
  
  window.addEventListener("resize", () => setResponsiveness());

  const addScrollable = () => {
    return "scrollable"
  }

  return (
    <> 
       {redirect()}
      <AppBar position="static" color="primary">
        <Tabs
          value={value}
          onChange={handleChange}        
          variant={mobileView?addScrollable():null}            
          scrollButtons="auto"  
          centered={!mobileView}   
        >
          <Tab label="Account Settings" {...a11yProps(0)} />
          <Tab label="My Events" {...a11yProps(1)} />
          <Tab label="Joint Events" {...a11yProps(2)} />
          <Tab label="Admin Settings" {...a11yProps(3)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <Container style={{textAlign:'center'}}>
        <Card>
          <CardContent>
            <h3>User Account settings</h3>
            <Divider/>
            <UpdateUsername/>
            <br/>
            <Divider/>
            <UpdatePassword/>
            <br/>
            <Divider/>
            <DeleteAccount/>
          </CardContent>
        </Card>
      </Container>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <UpdateEvent event={event}/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <JointEvents/>
      </TabPanel>
      <TabPanel value={value} index={3}>
        Admin Settings
      </TabPanel>
    </>
  );
}