import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { deleteAcc } from '../../store/actions/users_actions'
import { deleteAllEvents } from '../../store/actions/events_actions';
import { useDispatch } from 'react-redux';

export default function DeleteAccount() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const dispatch = useDispatch();

  const deleteAccount = () => {
    dispatch(deleteAllEvents()); 
    dispatch(deleteAcc());      
}

  return (
      
    <div>
        <Button 
            className="mt-3" 
            variant="outlined" 
            color="primary" 
            size="large"
            style={{width:"70%"}}
            onClick={()=> {
                handleClickOpen()
            }}
            >
                Delete Account
        </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">{"Account delete"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete account? It will be permanently deleted!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={deleteAccount} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    
    
  );
}