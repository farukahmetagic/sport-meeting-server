import React, {  } from 'react';
import { useFormik } from 'formik'
import * as Yup from 'yup';

import { useDispatch, useSelector } from 'react-redux';
import { updateUsername } from '../../store/actions/users_actions'
import { TextField, Button, Container, makeStyles, FormHelperText } from '@material-ui/core';

const UpdateUsername = () => {

    const dispatch = useDispatch();

    const users = useSelector(state=> state.users);

    let username = users.data.username;

    const formik = useFormik({
        initialValues:{username:''},
        validationSchema:Yup.object({
            username:Yup.string()
            .max(20, 'Must be 20 characters or less')
            .min(5, 'Must be 5 characters or more')
        }) ,
        onSubmit:(values,{resetForm})=>{
            handleSubmit(values)
        }
    });

    ///////////// STYLES //////////////////

    const useStyles = makeStyles(() => ({
        formControl: {
            textAlign: 'center'
        }
      }));   
    const classes = useStyles();    
    //////////////////////////////////////
    
    const handleSubmit = (values) => {
        dispatch(updateUsername(values))
    }

    const errorHelper = (formik, values) => ({
        error:formik.errors[values] && formik.touched[values] ? true:false,
        helperText:formik.errors[values] && formik.touched[values] ? formik.errors[values] : null
    });
    return(
        <>  
           <Container  maxWidth="sm" className={classes.formControl} >
                <form className="mt-3" onSubmit={formik.handleSubmit} key="updateUsername">
                    <div className="form-group">
                        <TextField
                            style={{width:'100%'}}
                            name="username"
                            label="New Username"
                            variant="outlined"
                            {...formik.getFieldProps('username')}
                            {...errorHelper(formik,'username')}
                        />
                        <FormHelperText>{`Current username - ${username}`}</FormHelperText>
                    </div>    
                    <Button 
                        className="mt-3" 
                        variant="contained" 
                        color="primary" 
                        type="submit"
                        size="large"
                        >
                            Update Username
                    </Button> 
                </form>                
           </Container>
        </>
    )
}

export default UpdateUsername;