import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik'
import * as Yup from 'yup';

import { useDispatch, useSelector } from 'react-redux';
import { registerUser, signInUser } from '../../store/actions/users_actions';
import { noEvent } from '../../store/actions/events_actions';
import { TextField, Button, Container, makeStyles, Card, CardContent } from '@material-ui/core';

const Auth = (props) => {

    const [ register, setRegister ] = useState(false);
    const notifications = useSelector( state => state.notifications )
    const dispatch = useDispatch();

    const formik = useFormik({
        initialValues:{username:'',password:''},
        validationSchema:Yup.object({
            username:Yup.string()
            .required('Sorry username is required')
            .max(20, 'Must be 20 characters or less')
            .min(5, 'Must be 5 characters or more'),
            password:Yup.string()
            .min(5, 'Must be 5 characters or more')
            .required('Sorry the password is required')
        }) ,
        onSubmit:(values,{resetForm})=>{
            handleSubmit(values)
        }
    });

    ///////////// STYLES //////////////////

    const useStyles = makeStyles(() => ({
        formControl: {
            display: "flex",
            flexDirection: "column",
            textAlign: 'center'
        }
      }));   
    const classes = useStyles();    
    //////////////////////////////////////
    
    const handleSubmit = (values) => {
        if(register){
            dispatch(registerUser(values))
            dispatch(noEvent())
        }else{
            dispatch(signInUser(values))
            dispatch(noEvent())
        }
    }

    const errorHelper = (formik, values) => ({
        error:formik.errors[values] && formik.touched[values] ? true:false,
        helperText:formik.errors[values] && formik.touched[values] ? formik.errors[values] : null
    });
    useEffect(() => {
        if(notifications && notifications.success){
            props.history.push('/')
        }
    },[notifications, props.history])
    return(        
        <Container  maxWidth="sm" style={{marginTop:"92px"}}>
            <Card>
                <CardContent>
                     <h1>{register ? 'Register':'Login'}</h1>
                     <form className={`mt-3 ${classes.formControl}`} onSubmit={formik.handleSubmit}>
                         <div className="form-group">
                             <TextField
                                 style={{width:'100%'}}
                                 name="username"
                                 label="Username"
                                 variant="outlined"
                                 {...formik.getFieldProps('username')}
                                 {...errorHelper(formik,'username')}
                             />
                         </div>    
                         <div className="form-group">
                             <TextField
                                 style={{width:'100%'}}
                                 name="password"
                                 label="Password"
                                 variant="outlined"
                                 type="password"
                                 {...formik.getFieldProps('password')}
                                 {...errorHelper(formik,'password')}
                             />
                         </div>   
                         <Button className="mt-3" variant="contained" color="primary" type="submit"
                         size="large">
                             {register ? 'Register':'Login'}
                         </Button> 
                         <Button 
                             className="mt-3"
                             variant="outlined" 
                             color="secondary" 
                             size="small"
                             onClick={() => setRegister(!register)}>
                             Want to {!register ? 'Register':'Login'} ?
                         </Button> 
                     </form> 
                 </CardContent>
             </Card>               
        </Container>        
    )
}

export default Auth;