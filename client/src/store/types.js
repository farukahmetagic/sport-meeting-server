export const ERROR_GLOBAL = 'error_global';
export const SUCCESS_GLOBAL = 'success_global';
export const CLEAR_NOTIFICATION = 'clear_notification';
export const SIGN_OUT = 'sign_out';

export const GET_SPORTS = 'get_sports';

export const GET_USER_EVENT = "get_user_event";
export const  GET_JOINT_EVENT = "get_joint_event";
export const GET_ALL_EVENTS = "get_all_events";
export const CLEAR_EVENT = "clear_event";
export const CLEAR_EVENTS = "clear_events";



export const AUTH_USER = 'auth_user';