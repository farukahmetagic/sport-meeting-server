import {  
    ERROR_GLOBAL,
    SUCCESS_GLOBAL,
    CLEAR_NOTIFICATION,
    AUTH_USER ,
    SIGN_OUT,
    GET_SPORTS,
    GET_USER_EVENT,
    CLEAR_EVENT,
    GET_ALL_EVENTS,
    CLEAR_EVENTS,
    GET_JOINT_EVENT
} from '../types';

/////// notification /////////////

export const errorGlobal = (msg) => ({
    type: ERROR_GLOBAL,
    payload: msg
});

export const successGlobal = (msg) => ({
    type: SUCCESS_GLOBAL,
    payload: msg
});

export const clearNotification = () => {
    return (dispatch) => {
        dispatch({
            type: CLEAR_NOTIFICATION
        })
    }
}


export const authUser = (user) => ({
    type: AUTH_USER,
    payload: user
});

export const signOut = () =>({
    type:SIGN_OUT
})

export const getSports = (data) => ({
    type: GET_SPORTS,
    payload: data
});

export const getEvent = (data) => ({
    type: GET_USER_EVENT,
    payload: data
});
export const getJointEvents = (data) => ({
    type: GET_JOINT_EVENT,
    payload: data
});

export const getEvents = (data) => ({
    type: GET_ALL_EVENTS,
    payload: data
});

export const clearEvent = () =>({
    type:CLEAR_EVENT
})
export const clearEvents = () =>({
    type:CLEAR_EVENTS
})

 