import * as users from './index';
import axios from 'axios';
import { getAuthHeader, removeTokenCookie } from '../../utils/tools';


axios.defaults.headers.post['Content-Type'] = 'application/json';


export const registerUser = (values) => {
    return async(dispatch) => {
        try{
            const user = await axios.post('/users/register',{
                username: values.username,
                password: values.password
            });
            dispatch(users.authUser({data: user.data, auth: true}))
            dispatch(users.successGlobal('Successfully created account !'))
        }catch(error){
            dispatch(users.errorGlobal(error.response.data.message))
        }
    }
}

export const signInUser = (values) => {
    return async(dispatch) => {
        try{
            const user = await axios.post('/users/signin',{
                username: values.username,
                password: values.password
            });
            dispatch(users.authUser({data: user.data, auth: true}))
            dispatch(users.successGlobal('Welcome !'))
        }catch(error){
            dispatch(users.errorGlobal(error.response.data.message))
        }
    }
}

export const updateUsername = (values) => {
    return async(dispatch) => {
        try{
            const user = await axios.patch('/users/profile/username',{
                username:values.username
            },getAuthHeader());
            dispatch(users.authUser({data: user.data, auth: true}))
            dispatch(users.successGlobal(`Success! New username: ${values.username}`))
        }catch(error){
            dispatch(users.errorGlobal(error.response.data.message))
        }
    }
}

export const updatePassword = (values) => {
    return async(dispatch) => {
        try{
            const user = await axios.patch('/users/profile/password',{
                password:values.password
            },getAuthHeader());
            dispatch(users.authUser({data: user.data, auth: true}))
            dispatch(users.successGlobal('Password updated successfully!'))
        }catch(error){
            dispatch(users.errorGlobal(error.response.data.message))
        }
    }
}

export const isAuthUser = () => {
    return async(dispatch) => {
        try{
            const user = await axios.get('/users/isauth', getAuthHeader());
            dispatch(users.authUser({data: user.data, auth: true}))
        }catch(error){
            dispatch(users.authUser({data: {}, auth: false}))
        }
    }
}

export const signOut = () => {
    return async (dispatch) => {
        removeTokenCookie();
        dispatch(users.signOut())
    }
}

export const sportsFields = (sport) => {
    return async(dispatch) => {
        try{
            const sportPlaces = await axios.get(`/sports/${sport}`)
            dispatch(users.getSports({data: sportPlaces.data}))
        }catch(error){
            dispatch(users.getSports({data: {}}))
        }
    }
}

export const dis = (sport, district) => {
    return async(dispatch) => {
        try{
            const districts = await axios.get(`/sports/${sport}/${district}`)
            dispatch(users.getSports({data: districts.data}))
        }catch(error){
            dispatch(users.getSports({data: {}}))
        }
    }
}

export const submitMeeting = (values) => {
    return async(dispatch) => {
        try{
            await axios.post('/events/myevents/newevent',{
                user:values.user,
                sport:values.sport,
                bez:values.district,
                address:values.address,
                dateAndTime:values.selectedDate,
                team:values.team
            },getAuthHeader());

            dispatch(users.successGlobal('Success!'))
        }catch(error){
            dispatch(users.errorGlobal(error.response.data.message))
        }
    }
}

export const deleteAcc = () => {
    return async(dispatch) => {
        try{
            await axios.post('/users/profile/delete',{},getAuthHeader());
            
            removeTokenCookie();
            dispatch(users.signOut())
            dispatch(users.successGlobal('Successfully deleted account!'))
        }catch(error){
            dispatch(users.errorGlobal(error.response.data.message))
        }
    }
}
