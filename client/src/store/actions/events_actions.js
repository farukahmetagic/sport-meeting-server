import * as events from './index';
import axios from 'axios';
import { getTokenCookie } from '../../utils/tools';
 
axios.defaults.headers.post['Content-Type'] = 'application/json';

export const getEvent = () => {
    return async(dispatch)=>{
        try{
            const event = await axios({
                url:`/events/myevents`,
                method: 'get',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token':getTokenCookie()     
                }
            });

            dispatch(events.getEvent({data:event.data}))
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}
export const getJointEvents = () => {
    return async(dispatch)=>{
        try{
            const event = await axios({
                url:`/events/myevents/jointevents`,
                method: 'get',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token':getTokenCookie()     
                }
            });

            dispatch(events.getJointEvents({data:event.data}))
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}

export const getAllEvents = (sport) => {
    return async(dispatch)=>{
        try{
            const event = await axios({
                url:`/events`,
                method: 'post',
                data:{
                    sport:sport
                }
            });
            dispatch(events.getEvents({data:event.data}))
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}

export const deleteEvent = (_id) => {
    return async(dispatch)=>{
        try{
            await axios({
                url:`/events/myevents/delete`,
                method: 'post',
                data:{
                    _id:_id
                },
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token':getTokenCookie()     
                }
            });

            dispatch(events.clearEvent())
            dispatch(events.successGlobal('Event deleted'))
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}
export const deleteJointEvent = (data) => {
    return async(dispatch)=>{
        try{
            await axios({
                url:`/events/myevents/deletejoint`,
                method: 'post',
                data:{
                    _id:data._id,
                    user:data.user
                },
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token':getTokenCookie()     
                }
            });

            dispatch(events.clearEvent())
            dispatch(events.successGlobal('Participation canceled'))
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}
export const deleteAllEvents = () => {
    return async(dispatch)=>{
        try{
            await axios({
                url:`/events/myevents/deleteall`,
                method: 'post',
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token':getTokenCookie()     
                }
            });

            dispatch(events.clearEvent())
            dispatch(events.successGlobal('Events deleted'))
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}

export const addParticipant = (data) => {
    return async(dispatch)=>{
        try{
            await axios({
                url:`/events/participate`,
                method: 'post',
                data:{
                    _id:data._id,
                    user:data.user,
                    team:data.team
                },
                headers: {
                    'Content-Type':'application/json',
                    'x-access-token':getTokenCookie()     
                }
            });
            dispatch(events.successGlobal('Participant added'))
            dispatch(events.clearEvent())
        } catch(error){
            dispatch(events.errorGlobal(error.response.data.message))
        }
    }
}

export const noEvent = () => {
    return async (dispatch) => {
        dispatch(events.clearEvent())
    }
}
export const noEvents = () => {
    return async (dispatch) => {
        dispatch(events.clearEvents())
    }
}