import { combineReducers } from 'redux';
import users from './users_reducers';
import event from './event_reducers';
import events from './events_reducers';
import sports from './sports_reducers';
import notifications from './notifications_reducers';
import jointevents from './jointevents_reducers';

const appReducers = combineReducers({
    users,
    sports,
    event,
    events,
    jointevents,
    notifications
});

export default appReducers;