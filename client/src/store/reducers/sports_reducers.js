import { GET_SPORTS } from '../types';

let DEFAULT_SPORTS_STATE = {};

export default function sportsReducers(state=DEFAULT_SPORTS_STATE, action){
    switch(action.type){
        case GET_SPORTS:
            return {...state,
            data: [...action.payload.data]
        }
        default:
            return state
    }
}
