import { 
    GET_USER_EVENT,
    CLEAR_EVENT
 } from '../types';

let DEFAULT_EVENT_STATE = {};

export default function eventReducer(state=DEFAULT_EVENT_STATE,action){
    switch(action.type){
        case GET_USER_EVENT:
            return { 
                ...state,
                data:[...action.payload.data]
            }
        case CLEAR_EVENT:
            return{}
        default:
            return state
    }
}