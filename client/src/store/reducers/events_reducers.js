import { 
    GET_ALL_EVENTS,
    CLEAR_EVENTS
 } from '../types';

let DEFAULT_EVENTS_STATE = {};

export default function eventReducer(state=DEFAULT_EVENTS_STATE,action){
    switch(action.type){
        case GET_ALL_EVENTS:
            return { 
                ...state,
                data:[...action.payload.data]
            }
        case CLEAR_EVENTS:
            return {}
        default:
            return state
    }
}